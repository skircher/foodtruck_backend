package com.pictarine.foodtruck;

public enum FoodType {
    Veggie,
    Vegan,
    Classic,
    Halal,
}
