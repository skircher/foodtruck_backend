package com.pictarine.foodtruck.entities;


import com.pictarine.foodtruck.FacilityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="foodtrucks")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodTruck {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String applicant;
    //@Enumerated(EnumType.ORDINAL)
    private String facilityType;
    private Integer locationId;
    private String address;
    private Double latitude;
    private Double longitude;
    private String [] foodType;
    @Column(length = 512)
    private String description;
    private String [] nationalities;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "fooditem_by_foodtrucks",
            joinColumns = @JoinColumn(name = "fooditem_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "foodtruck_id",
                    referencedColumnName = "id"))
    private List<FoodItem> foodItems;
}
