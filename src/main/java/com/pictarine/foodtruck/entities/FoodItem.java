package com.pictarine.foodtruck.entities;

import com.pictarine.foodtruck.FoodHeat;
import com.pictarine.foodtruck.FoodState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="fooditems")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodItem {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Enumerated(EnumType.ORDINAL)
    private FoodState foodState;
    private String nationality;
    @Enumerated(EnumType.ORDINAL)
    private FoodHeat foodHeat;
}