package com.pictarine.foodtruck;

public enum FoodState {
    Drink,
    Solid
}
