package com.pictarine.foodtruck.controllers;

import com.pictarine.foodtruck.entities.FoodItem;
import com.pictarine.foodtruck.services.FoodItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/fooditems")
public class FoodItemController {
    private FoodItemService foodItemService;

    @Autowired
    FoodItemController(FoodItemService foodItemService){
        this.foodItemService = foodItemService;
    }

    /**
     * GET /fooditems
     * @return
     */
    @RequestMapping("")
    public List<FoodItem> getFoodItems() {
        return this.foodItemService.getFoodItems();
    }

    /**
     * POST /import
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseEntity<List<FoodItem>> importFoodItems(@RequestParam("file") MultipartFile file) throws IOException {
        for (FoodItem g:this.foodItemService.importFoodItems(file)) {
            this.foodItemService.saveFoodItems(g);
        }
        return new ResponseEntity<List<FoodItem>>(this.foodItemService.getFoodItems(), HttpStatus.CREATED);
    }
}
