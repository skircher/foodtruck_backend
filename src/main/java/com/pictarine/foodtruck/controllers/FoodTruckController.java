package com.pictarine.foodtruck.controllers;

import com.pictarine.foodtruck.entities.FoodItem;
import com.pictarine.foodtruck.entities.FoodTruck;
import com.pictarine.foodtruck.services.FoodTruckService;
import org.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController // Controller + ResponseBody
@RequestMapping("/api/v1/foodtruck")
public class FoodTruckController {
    private FoodTruckService foodTruckService;

    FoodTruckController(FoodTruckService foodTruckService){
        this.foodTruckService = foodTruckService;
    }

    /**
     * GET /foodtruck
     * @return
     */
    @RequestMapping("")
    public List<FoodTruck> getFoodTruck() {
        return this.foodTruckService.getFoodTrucks();
    }

    /**
     * GET /list/page
     * @return
     */
    @RequestMapping("/list/{page}/{facility}")
    public Page<FoodTruck> getFoodTruckPaged(@PathVariable Integer page,@PathVariable String facility ) {
        if(facility.contains("none")){
            return this.foodTruckService.getFoodTruckPagination(page);
        }
        else{
            return this.foodTruckService.getFoodTruckByFacilityTypePagination(facility,page);
        }

    }


    /**
     * GET /foodtruck/id
     * @return
     */
    @RequestMapping("/{id}")
    public FoodTruck getFoodTruckById(@PathVariable Long id) {
        return this.foodTruckService.findFoodTruckById(id);
    }


    @RequestMapping("/sync")
    public ResponseEntity<List<FoodTruck>> synchroFoodTruck() throws IOException, JSONException {
        return new ResponseEntity<List<FoodTruck>>(this.foodTruckService.syncFoodTruck(), HttpStatus.CREATED);
    }
}
