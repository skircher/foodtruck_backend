package com.pictarine.foodtruck;

public enum OriginFood {
    Japanese,
    Asian,
    American,
    Mexican,
    Peruvian,
    Italian,
    Chinese
}
