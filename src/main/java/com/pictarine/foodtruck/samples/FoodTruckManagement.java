package com.pictarine.foodtruck.samples;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class FoodTruckManagement {

    public FoodTruckManagement(){
    }
        /**
         * STREAM an URL
         * @param pathUrl
         * @return StringBuffer
         * @throws IOException
         */
        public StringBuffer timesheetJSonXml(String pathUrl) throws IOException {
            URL obj = new URL(pathUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            response.append("{\"foodtrucks\":");
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            response.append("}");
            in.close();
            return response;
        }
    }




