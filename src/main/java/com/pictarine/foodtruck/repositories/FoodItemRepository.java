package com.pictarine.foodtruck.repositories;

import com.pictarine.foodtruck.entities.FoodItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodItemRepository extends PagingAndSortingRepository<FoodItem, Long> {
}
