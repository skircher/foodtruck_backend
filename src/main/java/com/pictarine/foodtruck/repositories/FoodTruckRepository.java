package com.pictarine.foodtruck.repositories;

import com.pictarine.foodtruck.FacilityType;
import com.pictarine.foodtruck.entities.FoodTruck;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodTruckRepository extends PagingAndSortingRepository<FoodTruck, Long> {
    Page<FoodTruck> findAll(Pageable pageable);
    Page<FoodTruck> findByFacilityTypeContaining(String facilityType, Pageable pageable);
    Page<FoodTruck> findByNationalitiesContaining(String [] nationalities, Pageable pageable);
}
