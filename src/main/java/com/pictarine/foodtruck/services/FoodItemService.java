package com.pictarine.foodtruck.services;

import com.pictarine.foodtruck.FoodHeat;
import com.pictarine.foodtruck.FoodState;
import com.pictarine.foodtruck.entities.FoodItem;
import com.pictarine.foodtruck.repositories.FoodItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FoodItemService {

    FoodItemRepository foodItemRepository;

    @Autowired
    FoodItemService(FoodItemRepository foodItemRepository){
        this.foodItemRepository = foodItemRepository;
    }

    /**
     *
     * @return list of fooditem
     */
    public List<FoodItem> getFoodItems() {
        return (List<FoodItem>) this.foodItemRepository.findAll();
    }

    /**
     *
     * @param fi
     * @return fooditem save
     */
    public FoodItem saveFoodItems(FoodItem fi) {
        return this.foodItemRepository.save(fi);
    }

    /**
     * Import FoodItems in csv file
     * @param file csv
     * @return List of FoodItems present in csv file
     */
    public List<FoodItem> importFoodItems(MultipartFile file) throws IOException {
        String separator = ",";
        InputStream is = file.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<FoodItem> listG = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] array = line.split(separator);
            FoodItem g = new FoodItem();
            g.setName(array[0]);
            g.setFoodState(FoodState.valueOf(array[1]));
            if(!array[2].contains("Empty"))
                g.setNationality(array[2]);
            g.setFoodHeat(FoodHeat.valueOf(array[3]));
            listG.add(g);
        }return listG;
    }
}
