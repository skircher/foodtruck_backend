package com.pictarine.foodtruck.services;

import com.pictarine.foodtruck.FacilityType;
import com.pictarine.foodtruck.FoodType;
import com.pictarine.foodtruck.OriginFood;
import com.pictarine.foodtruck.entities.FoodItem;
import com.pictarine.foodtruck.entities.FoodTruck;
import com.pictarine.foodtruck.repositories.FoodTruckRepository;
import com.pictarine.foodtruck.samples.FoodTruckManagement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FoodTruckService {
    FoodTruckRepository foodTruckRepository;
    FoodItemService foodItemService;

    @Autowired
    FoodTruckService(FoodTruckRepository foodTruckRepository, FoodItemService foodItemService){
        this.foodTruckRepository = foodTruckRepository;
        this.foodItemService = foodItemService;
    }

    /**
     *
     * @return list of foodtrucks
     */
    public List<FoodTruck> getFoodTrucks() {
        return (List<FoodTruck>) this.foodTruckRepository.findAll();
    }

    /**
     *
     * @return paged foodtruck list
     */
    public Page<FoodTruck> getFoodTruckPagination(Integer page) {
        Pageable paginationFoodTruckAll = PageRequest.of(page, 6);
        return this.foodTruckRepository.findAll(paginationFoodTruckAll);
    }

    /**
     *
     * @param id
     * @return foodtruck thanks to Id
     */
    public FoodTruck findFoodTruckById(Long id) {
        return this.foodTruckRepository.findById(id).orElseThrow();
    }

    /**
     *
     * @param facility, page
     * @return foodtruck thanks to Id
     */
    public Page<FoodTruck> getFoodTruckByFacilityTypePagination(String facility, Integer page) {
        Pageable paginationFoodTruckAll = PageRequest.of(page, 6);
        return this.foodTruckRepository.findByFacilityTypeContaining(facility,paginationFoodTruckAll);
    }

    /**
     *
     * @param nationality, page
     * @return foodtruck thanks to Id
     */
    public Page<FoodTruck> getFoodTruckByNationalitiesPagination(String nationality, Integer page) {
        Pageable paginationFoodTruckAll = PageRequest.of(page, 6);
        String [] nat = {nationality} ;

        return this.foodTruckRepository.findByNationalitiesContaining(nat,paginationFoodTruckAll);
    }

    /**
     *
     * @param ft
     * @return foodtruck save
     */
    public FoodTruck saveFoodTuck(FoodTruck ft) {
        return this.foodTruckRepository.save(ft);
    }

    /**
     * Allows to parse JSON StreemBuffer to FoodTruck Entities
     * @return List de FoodTruck
     * @throws IOException
     * @throws JSONException
     */
    public List<FoodTruck> syncFoodTruck() throws IOException, JSONException {
        List<FoodTruck> ListFt = new ArrayList<>();
        List<FoodItem> listFi = this.foodItemService.getFoodItems();
        String projectUrl = "https://data.sfgov.org/resource/rqzj-sfat.json";
        //Use class and feature which allow to Stream an URL
        FoodTruckManagement foodTruckManagement = new FoodTruckManagement();
        //Allows to parse StringBuffer to Json Object
        JSONObject timesheet_JSONObject = new JSONObject(foodTruckManagement.timesheetJSonXml(projectUrl).toString());

        JSONArray foodtrucks_JSONArray = timesheet_JSONObject.getJSONArray("foodtrucks");
        for (int i = 0; i < foodtrucks_JSONArray.length() ; i++) {
            FoodTruck ft = new FoodTruck();
            JSONObject tasks_JSONObject = foodtrucks_JSONArray.getJSONObject(i);
            ft.setApplicant(tasks_JSONObject.getString("applicant"));
            ft.setLocationId(Integer.parseInt(tasks_JSONObject.getString("objectid")));
            ft.setAddress(tasks_JSONObject.getString("address"));
            ft.setLatitude(Double.parseDouble(tasks_JSONObject.getString("latitude")));
            ft.setLongitude(Double.parseDouble(tasks_JSONObject.getString("longitude")));

            //Import facility type
            if(tasks_JSONObject.has("facilitytype")){
                //ft.setFacilityType(FacilityType.valueOf(tasks_JSONObject.getString("facilitytype").replace(' ','_')));
                ft.setFacilityType(tasks_JSONObject.getString("facilitytype").replace(' ','_'));
            }

            if(tasks_JSONObject.has("fooditems")) {
                //Import food type
                String foodItem = tasks_JSONObject.getString("fooditems");
                ft.setDescription(foodItem);
                List<String> typeFood = new ArrayList<String>() ;
                typeFood.add(FoodType.Classic.toString());
                if(foodItem.toUpperCase().contains("VEGI") || foodItem.toUpperCase().contains("VEGGIE") || foodItem.toUpperCase().contains("VEGETARIAN") | foodItem.toUpperCase().contains("VEGE"))
                    typeFood.add(FoodType.Veggie.toString());
                if(foodItem.toUpperCase().contains("VEGAN") || foodItem.toUpperCase().contains("VEGANA"))
                    typeFood.add(FoodType.Vegan.toString());
                if(foodItem.toUpperCase().contains("HALAL"))
                    typeFood.add(FoodType.Halal.toString());
                String[] stockArr = new String[typeFood.size()];
                stockArr = typeFood.toArray(stockArr);
                ft.setFoodType(stockArr);

                List<FoodItem> foodItemByTruck = new ArrayList<>();
                //import list of food
                for (FoodItem food : listFi ){
                    if(foodItem.toUpperCase().contains(food.getName().toUpperCase())){
                        foodItemByTruck.add(food);
                    }
                }
                ft.setFoodItems(foodItemByTruck);

                //Import nationalities food
                List<String> nationalities = new ArrayList<String>();
                for(OriginFood nationality : OriginFood.values()){
                    if(foodItem.toUpperCase().contains(nationality.toString().toUpperCase()))
                        nationalities.add(nationality.toString());
                }

                List<String> nationalityByFood = foodItemByTruck.stream().map(FoodItem::getNationality).collect(Collectors.toList());
                for(String natFood : nationalityByFood){
                    if(!nationalities.contains(natFood) && natFood!=null)
                        nationalities.add(natFood);
                }

                String[] stockNat = new String[nationalities.size()];
                stockNat = nationalities.toArray(stockNat);
                ft.setNationalities(stockNat);
            }


            //System.out.println(tasks_JSONObject.getString("applicant"));

            this.saveFoodTuck(ft);
            ListFt.add(ft);
        }
        return ListFt;
    }
}
