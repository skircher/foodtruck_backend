CREATE TABLE fooditem_by_foodtrucks (
    id serial PRIMARY KEY,
    foodtruck_id INT,
    FOREIGN KEY (foodtruck_id) REFERENCES FOODTRUCKS ON DELETE CASCADE,
    fooditem_id INT,
    FOREIGN KEY (fooditem_id) REFERENCES FOODITEMS ON DELETE CASCADE
)