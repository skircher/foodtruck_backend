CREATE TABLE fooditems(
    id serial PRIMARY KEY,
    name VARCHAR( 50 ) NOT NULL,
    stateFood BOOLEAN,
    nationality VARCHAR( 50 ),
    heat BOOLEAN
)