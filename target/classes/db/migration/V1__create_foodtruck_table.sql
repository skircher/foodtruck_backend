CREATE TABLE foodtrucks (
    id serial PRIMARY KEY,
    applicant VARCHAR( 150 ) NOT NULL,
    facilityType VARCHAR ( 25 ),
    locationId INTEGER NOT NULL,
    address VARCHAR ( 150 ) NOT NULL,
    latitude NUMERIC NOT NULL,
    longitude NUMERIC NOT NULL,
    foodType VARCHAR ( 25 )[],
    description VARCHAR ( 300 ),
    nationalities VARCHAR ( 25 )[]
)